---
title: Typography
---

Examples of typography as they are used throughout the digital experience. The elements are stacked apart in units of 10 pixels for a consistent vertical rhythm.

***


<h2 class="sf-heading-2">Standard font</h2>

The Inter UI is the standard font used across the website ([download](https://github.com/rsms/inter/releases/download/v3.3/Inter-3.3.zip)).

[View examples of the standard font](typography-standard).

<div class="s-pattern-grid">

<div class="s-pattern-grid__item s-typography-demo s-typography-demo--regular">
Inter UI Regular <br />aAbBdDeEgGmMoOrRsS 1234567890!?()"'@&amp;.,
</div>

<div class="s-pattern-grid__item s-typography-demo s-typography-demo--regular">
<em>Inter UI Italic <br />aAbBdDeEgGmMoOrRsS 1234567890!?()"'@&amp;.,</em>
</div>

<div class="s-pattern-grid__item s-typography-demo s-typography-demo--medium">
Inter UI Medium <br />aAbBdDeEgGmMoOrRsS 1234567890!?()"'@&amp;.,
</div>

<div class="s-pattern-grid__item s-typography-demo s-typography-demo--regular">
<em>Inter UI Medium Italic <br />aAbBdDeEgGmMoOrRsS 1234567890!?()"'@&amp;.,</em>
</div>

<div class="s-pattern-grid__item s-typography-demo s-typography-demo--bold">
Inter UI Bold <br />aAbBdDeEgGmMoOrRsS 1234567890!?()"'@&amp;.,
</div>

</div>

***

<h2 class="sf-heading-2">Variant fonts</h2>

Playfair Display ([download](https://fonts.google.com/specimen/Playfair+Display?selection.family=Playfair+Display)) and PT Sans ([download](https://fonts.google.com/specimen/PT+Sans)) are used on promotional or landing pages.

[View examples of the variant fonts](typography-variant-1).

<div class="s-pattern-grid">

<div class="s-pattern-grid__item s-typography-demo s-typography--variant-1-demo s-typography-variant-1-demo--regular">
Playfair Display Regular <br />aAbBdDeEgGmMoOrRsS 1234567890!?()"'@&amp;.,
</div>

<div class="s-pattern-grid__item s-typography-demo s-typography--variant-1-demo s-typography-variant-1-demo--bold">
Playfair Display Bold <br />aAbBdDeEgGmMoOrRsS 1234567890!?()"'@&amp;.,
</div>

<div class="s-pattern-grid__item"></div>

<div class="s-pattern-grid__item s-typography-demo s-typography--variant-2-demo s-typography-variant-2-demo--regular">
PT Sans Regular <br />aAbBdDeEgGmMoOrRsS 1234567890!?()"'@&amp;.,
</div>

<div class="s-pattern-grid__item s-typography-demo s-typography--variant-2-demo s-typography-variant-2-demo--italic">
<em>PT Sans Italic <br />aAbBdDeEgGmMoOrRsS 1234567890!?()"'@&amp;.,</em>
</div>

<div class="s-pattern-grid__item s-typography-demo s-typography--variant-2-demo s-typography-variant-2-demo--bold">
PT Sans Bold <br />aAbBdDeEgGmMoOrRsS 1234567890!?()"'@&amp;.,
</div>

</div>
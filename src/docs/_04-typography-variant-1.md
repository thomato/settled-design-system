---
title: Variant typography examples
---

This uses PT Sans as the font for the copy Playfair Display for the headings.

***

<h2 class="sf-heading-2">Heading styles</h2>

<h3 class="sf-heading-3">H1 variant heading style</h3>

<div class="sf-background-variant-1-primary">
<h1 class="s-typography-variant-1-heading-1">This is a variant H1 heading style</h1>
</div>

```
{{render '@typography--variant-1-heading-1' }}
```

<h3 class="sf-heading-3">H2 variant heading style</h3>

<h2 class="s-typography-variant-1-heading-2">This is a variant H2 heading style</h2>

```
{{render '@typography--variant-1-heading-2' }}
```

<h3 class="sf-heading-3">H3 variant heading style</h3>

<h3 class="s-typography-variant-1-heading-3">This is a variant H3 heading style</h3>

```
{{render '@typography--variant-1-heading-3' }}
```

***

<h2 class="sf-heading-2">Copy styles</h2>

<h3 class="sf-heading-3">Paragraph style</h3>

<p class="s-typography-variant-1-body">This is the style for variant copy.</p>

```
{{render '@typography--variant-1-body' }}
```

<h3 class="sf-heading-3">Link style</h3>

<p class="s-typography-variant-1-body"><a href="#">This is a link</a></p>

```
{{render '@typography--variant-1-body-link' }}
```

<h3 class="sf-heading-3">Small text</h3>

<p class="s-typography-variant-1-body-small">This is the style for variant small text.</p>

```
{{render '@typography--variant-1-body-small' }}
```

<h3 class="sf-heading-3">Quotations</h3>

<blockquote class="s-quote-variant">

  <p>This is an example of a quote.</p>

  <cite>This is the citation</cite>

</blockquote>

```
{{render '@typography--variant-1-blockquote' }}
```


***

<h2 class="sf-heading-2">Collated demo</h2>

<div class="sf-background-variant-1-primary">
<h1 class="s-typography-variant-1-heading-1">This is a H1 heading style</h1>
</div>

<p class="s-typography-body">This is a paragraph.</p>

<h2 class="s-typography-variant-1-heading-2">This is a H2 heading style</h2>

<p class="s-typography-body">This is a paragraph.</p>

<h3 class="s-typography-variant-1-heading-3">This is a H3 heading style</h3>

<p class="s-typography-variant-1-body">This is a paragraph. <strong>This is bold (strong).</strong> <em>This is italic (em).</em> <a href="#">This is a link</a>.</p>

<ul class="s-typography-variant-1-body">
  <li>A list item; this is number one.</li>
  <li>A list item; this is number two.</li>
  <li>A list item; this is number three.</li> 
</ul>

<blockquote class="s-quote-variant">

  <p>This is an example of a quote.</p>

  <cite>This is the citation</cite>

</blockquote>

<p class="s-typography-body">This is a closing paragraph.</p>

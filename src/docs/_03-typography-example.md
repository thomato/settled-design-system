---
title: Typography example
---

A hidden page to test the typographic layout.

***

<p class="sf-breakpoint"></p>

<h1 class="s-typography-heading-1">This is a H1 heading style <br />wrapping to view line-height</h1>

<p class="s-typography-body">This is a paragraph.</p>

<h2 class="s-typography-heading-2">This is a H2 heading style <br />wrapping to view line-height</h2>

<p class="s-typography-body">This is a paragraph.</p>

<h3 class="s-typography-heading-3">This is a H3 heading style <br />wrapping to view line-height</h3>

<p class="s-typography-body">This is a paragraph.</p>

<h4 class="s-typography-heading-4">This is a H4 heading style <br />wrapping to view line-height</h4>

<p class="s-typography-body">This is a paragraph.</p>

<h5 class="s-typography-heading-5">This is a H5 heading style <br />wrapping to view line-height</h5>

<p class="s-typography-body">This is a paragraph.</p>

<p class="s-typography-body"><strong>This is bold (strong).</strong></p>

<p class="s-typography-body"><em>This is italic (em).</em></p>

<p class="s-typography-body">This is a paragraph with <a href="#">a link</a> and<br />it wraps to another line.</p>

<ul class="s-typography-body">
  <li>A list item; this is number one.</li>
  <li>A list item; this is number two.</li>
  <li>A list item; this is number three.</li>
  <li>A list item; this is number four.</li>
  <li>A list item; this is number five and <br />wraps next to a new line.</li>
</ul>
---
title: Settled Design System
---

The Settled Design System is the single source of truth which groups all the elements that will allow the teams to design, realise and develop a product.

***

<h2 class="sf-heading-2">Material Design</h2>

The UI is built on [Vue Material](https://vuematerial.io/) which is a [Vue.js](https://vuejs.org/) component library that implements Google's [Material Design](https://material.io/).

<!--
{{#componentList}}
<a href="{{path '/components/preview/{{ this.handle }}' }}">{{ this.title }}</a>
{{/componentList}}  
-->
---
title: Grid
---

Components are aligned to a responsive grid for small, medium, and large screen sizes. 

***

<h2 class="sf-heading-2">Responsive example</h2>

<p class="sf-breakpoint-grid"></p>

<div class="sf-layout sf-layout--demo">
<div class="sf-layout-item"></div>
<div class="sf-layout-item"></div>
<div class="sf-layout-item"></div>
<div class="sf-layout-item"></div>
<div class="sf-layout-item sf-layout-item--medium"></div>
<div class="sf-layout-item sf-layout-item--medium"></div>
<div class="sf-layout-item sf-layout-item--medium"></div>
<div class="sf-layout-item sf-layout-item--medium"></div>
<div class="sf-layout-item sf-layout-item--large"></div>
<div class="sf-layout-item sf-layout-item--large"></div>
<div class="sf-layout-item sf-layout-item--large"></div>
<div class="sf-layout-item sf-layout-item--large"></div>
</div>

***

<h2 class="sf-heading-2">Small screen</h2>

The small screen grid has 4 columns with gutters and margins set at 16 pixels.

<div class="sf-layout sf-layout--small">
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
</div>

***

<h2 class="sf-heading-2">Medium screen</h2>

The medium screen grid has 6 columns with gutters and margins set at 16 pixels.

<div class="sf-layout sf-layout--medium">
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
</div>

***

<h2 class="sf-heading-2">Large screen</h2>

The large screen grid has 12 columns with gutters and margins set at 24 pixels.

<div class="sf-layout sf-layout--large">
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
<div class="sf-layout-item">
</div>
</div>

---
title: Standard typography examples
---

This uses Inter UI as the font for the copy and all headings.

***

<h2 class="sf-heading-2">Heading styles</h2>

<h3 class="sf-heading-3">H1 heading style</h3>

This has a large top margin and should be used as a page title.

<h1 class="s-typography-heading-1">This is a H1 heading style</h1>

```
{{render '@typography--heading-1' }}
```

<h3 class="sf-heading-3">H2 heading style</h3>

This is a subheading and should be used to divide the main sections of a page.

<h2 class="s-typography-heading-2">This is a H2 heading style</h2>

```
{{render '@typography--heading-2' }}
```

<h3 class="sf-heading-3">H3 heading style</h3>

This should be used to further divide subsections.

<h3 class="s-typography-heading-3">This is a H3 heading style</h3>

```
{{render '@typography--heading-3' }}
```

<h3 class="sf-heading-3">H4 heading style</h3>

<h4 class="s-typography-heading-4">This is a H4 heading style</h4>

```
{{render '@typography--heading-4' }}
```

<h3 class="sf-heading-3">H5 heading style</h3>

<h5 class="s-typography-heading-5">This is a H5 heading style</h5>

```
{{render '@typography--heading-5' }}
```

***

<h2 class="sf-heading-2">Copy styles</h2>

<h3 class="sf-heading-3">Paragraph style</h3>

<p class="s-typography-body">This is the style for copy.</p>

```
{{render '@typography--body' }}
```

<h3 class="sf-heading-3">Link style</h3>

<p class="s-typography-body"><a href="#">This is a link</a></p>

```
{{render '@typography--body-link' }}
```

***

<h2 class="sf-heading-2">Collated demo</h2>

<h1 class="s-typography-heading-1">This is a H1 heading style</h1>

<p class="s-typography-body">This is a paragraph.</p>

<h2 class="s-typography-heading-2">This is a H2 heading style</h2>

<p class="s-typography-body">This is a paragraph.</p>

<h3 class="s-typography-heading-3">This is a H3 heading style</h3>

<p class="s-typography-body">This is a paragraph.</p>

<h4 class="s-typography-heading-4">This is a H4 heading style</h4>

<p class="s-typography-body">This is a paragraph.</p>

<h5 class="s-typography-heading-5">This is a H5 heading style</h5>

<p class="s-typography-body">This is a paragraph. <strong>This is bold (strong).</strong> <em>This is italic (em).</em> <a href="#">This is a link</a>.</p>

<ul class="s-typography-body">
  <li>A list item; this is number one.</li>
  <li>A list item; this is number two.</li>
  <li>A list item; this is number three.</li> 
</ul>

<p class="s-typography-body">This is a closing paragraph.</p>

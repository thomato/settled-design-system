---
title: Colour
---

Brand palette and usage.

***

<h2 class="s-typography-heading-2">Primary colours</h2>

<section class="s-palette">

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #EF5350;">&#35;EF5350</span>
    <span class="s-palette__name">Coral</span>
    <span class="s-palette__sass-variable">$s-color-primary</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #4B4F65;">&#35;4B4F65</span>
    <span class="s-palette__name">Denim</span>
    <span class="s-palette__sass-variable">$s-color-secondary</span>
</div>

</section>

***

<h2 class="s-typography-heading-2">Coral variations</h2>

<section class="s-palette">

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #C93331;">&#35;C93331</span>
    <span class="s-palette__name">Coral 900</span>
    <span class="s-palette__sass-variable">$s-color-primary-900</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #D73E3D;">&#35;D73E3D</span>
    <span class="s-palette__name">Coral 800</span>
    <span class="s-palette__sass-variable">$s-color-primary-800</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #E54444;">&#35;E54444</span>
    <span class="s-palette__name">Coral 700</span>
    <span class="s-palette__sass-variable">$s-color-primary-700</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #F84E4B;">&#35;F84E4B</span>
    <span class="s-palette__name">Coral 600</span>
    <span class="s-palette__sass-variable">$s-color-primary-600</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #FF554B;">&#35;FF554B</span>
    <span class="s-palette__name">Coral 500</span>
    <span class="s-palette__sass-variable">$s-color-primary-500</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #EF5350;">&#35;EF5350</span>
    <span class="s-palette__name">Primary</span>
    <span class="s-palette__sass-variable">$s-color-primary</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #F18184;">&#35;F18184</span>
    <span class="s-palette__name">Coral 300</span>
    <span class="s-palette__sass-variable">$s-color-primary-300</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #F7A4A7;">&#35;F7A4A7</span>
    <span class="s-palette__name">Coral 200</span>
    <span class="s-palette__sass-variable">$s-color-primary-200</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #FFD3D9;">&#35;FFD3D9</span>
    <span class="s-palette__name">Coral 100</span>
    <span class="s-palette__sass-variable">$s-color-primary-100</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #FFEEF1;">&#35;FFEEF1</span>
    <span class="s-palette__name">Coral 50</span>
    <span class="s-palette__sass-variable">$s-color-primary-50</span>
</div>

</section>

***

<h2 class="s-typography-heading-2">Denim variations</h2>

<section class="s-palette">

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #373A4D;">&#35;373A4D</span>
    <span class="s-palette__name">Demin 900</span>
    <span class="s-palette__sass-variable">$s-color-secondary-900</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #4B4F65;">&#35;4B4F65</span>
    <span class="s-palette__name">Demin 800</span>
    <span class="s-palette__sass-variable">$s-color-secondary-800</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #4B4F65;">&#35;4B4F65</span>
    <span class="s-palette__name">Demin 700</span>
    <span class="s-palette__sass-variable">$s-color-secondary-700</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #6F7693;">&#35;6F7693</span>
    <span class="s-palette__name">Demin 600</span>
    <span class="s-palette__sass-variable">$s-color-secondary-600</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #7E85A5;">&#35;7E85A5</span>
    <span class="s-palette__name">Demin 500</span>
    <span class="s-palette__sass-variable">$s-color-secondary-500</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #4B4F65;">&#35;4B4F65</span>
    <span class="s-palette__name">Secondary</span>
    <span class="s-palette__sass-variable">$s-color-secondary</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #A5AAC5;">&#35;A5AAC5</span>
    <span class="s-palette__name">Demin 300</span>
    <span class="s-palette__sass-variable">$s-color-secondary-300</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #BEC3D9;">&#35;BEC3D9</span>
    <span class="s-palette__name">Demin 200</span>
    <span class="s-palette__sass-variable">$s-color-secondary-200</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #D6DBED;">&#35;D6DBED</span>
    <span class="s-palette__name">Demin 100</span>
    <span class="s-palette__sass-variable">$s-color-secondary-100</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #F0EFFD;">&#35;F0EFFD</span>
    <span class="s-palette__name">Demin 50</span>
    <span class="s-palette__sass-variable">$s-color-secondary-50</span>
</div>

</section>

***

<h2 class="s-typography-heading-2">Panel colours</h2>

<section class="s-palette">

<div class="s-palette__colour s-palette__colour--light">
    <span class="s-palette__visual" style="background-color: #F5F5F5;">&#35;F5F5F5</span>
    <span class="s-palette__name">Panel dark</span>
    <span class="s-palette__sass-variable">$s-color-panel-accent-1</span>
</div>

<div class="s-palette__colour s-palette__colour--light">
    <span class="s-palette__visual" style="background-color: #FAFAFA;">&#35;FAFAFA</span>
    <span class="s-palette__name">Panel light</span>
    <span class="s-palette__sass-variable">$s-color-panel-accent-2</span>
</div>

<div class="s-palette__colour s-palette__colour--light">
    <span class="s-palette__visual" style="background-color: #FFF;">&#35;FFF</span>
    <span class="s-palette__name">Background</span>
    <span class="s-palette__sass-variable">$s-color-panel</span>
</div>

</section>

***

<h2 class="s-typography-heading-2">Landing page colours</h2>

To be used for the new landing page designs.

<section class="s-palette">

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #EE7768;">&#35;EE7768</span>
    <span class="s-palette__name">Variant primary</span>
    <span class="s-palette__sass-variable">$s-color-variant-primary</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #409297;">&#35;409297</span>
    <span class="s-palette__name">Variant secondary</span>
    <span class="s-palette__sass-variable">$s-color-variant-secondary</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #424242;">&#35;424242</span>
    <span class="s-palette__name">Strapline</span>
    <span class="s-palette__sass-variable">$s-color-variant-ink-1</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #757575;">&#35;757575</span>
    <span class="s-palette__name">Copy</span>
    <span class="s-palette__sass-variable">$s-color-variant-ink-2</span>
</div>

<div class="s-palette__colour s-palette__colour--dark">
    <span class="s-palette__visual" style="background-color: #816867;">&#35;816867</span>
    <span class="s-palette__name">Small copy</span>
    <span class="s-palette__sass-variable">$s-color-variant-ink-3</span>
</div>

</section>
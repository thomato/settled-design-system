'use strict';

const mandelbrot = require('@frctl/mandelbrot');

const settledFractalSubtheme = mandelbrot({
  nav: ['docs', 'components'],
  panels: ['html', 'resources', 'notes'],
  skin: 'default',
  styles: ['/_settled-fractal-subtheme/styles/site.css']
});

settledFractalSubtheme.addLoadPath(__dirname + '/views');

settledFractalSubtheme.addStatic(__dirname + '/assets', '/_settled-fractal-subtheme');

module.exports = settledFractalSubtheme;
const gulp = require('gulp');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
const sourcemaps = require('gulp-sourcemaps');
const fractal = require('@frctl/fractal').create();
const logger = fractal.cli.console;

/*
 * Settled - project settings.
 */
fractal.set('project.title', 'Settled Design System ');
fractal.set('project.version', '1.0.0');


/*
 * Tell Fractal where to look for components.
 */
fractal.components.set('path', `${__dirname}/src/components`);


/*
 * Tell Fractal where to look for documentation pages.
 */
fractal.docs.set('path', `${__dirname}/src/docs`);

const hbs = require('@frctl/handlebars')({
    helpers: {
        componentList: function() {
            let ret = "<ul>";
            const options = Array.from(arguments).pop();
            for (let component of fractal.components.flatten()) {
                ret = ret + "<li>" + options.fn(component.toJSON()) + "</li>";
            }
            return ret + "</ul>";
        }
    }
});

fractal.docs.engine(hbs);

/*
 * Tell the Fractal web preview plugin where to look for static assets.
 */
fractal.web.set('static.path', `${__dirname}/public`);
fractal.web.set('builder.dest', `${__dirname}/build`);


/*
 * Rename "docs" to "foundation".
 */
fractal.docs.set('label', 'Foundations');
fractal.docs.set('title', 'Foundations');


/*
 * The default "overview" page under the docs.
 */
fractal.docs.set('indexLabel', 'Overview');


/*
 * Settled - apply custom skin.
 */
const settledSubtheme = require('./_settled-fractal-subtheme');
fractal.web.theme(settledSubtheme);

// This allows access to the settled CSS from the Fractal theme.
fractal.web.set('static.path', __dirname + '/public');

/*
 * Gulp tasks
 */

gulp.task('build', function() {
	gulp.src([`${__dirname}/src/global/styles/index.scss`])
	.pipe(sassGlob())
    .pipe(sourcemaps.init())
	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
	.pipe(rename('settled.min.css'))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(`${__dirname}/build/styles`));
    
    gulp.src([
        `${__dirname}/src/global/fonts/**/*`
    ])
    .pipe(gulp.dest(`${__dirname}/build/fonts`));
});

gulp.task('styles:start', function() {
	gulp.src([`${__dirname}/src/global/styles/index.scss`])
	.pipe(sassGlob())
    .pipe(sourcemaps.init())
	.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
	.pipe(rename('settled.css'))
    .pipe(sourcemaps.write('./maps'))
	.pipe(gulp.dest(`${__dirname}/public/styles`));
});

gulp.task('styles:watch', function(){
    gulp.watch([
            `${__dirname}/src/global/**/*.scss`, 
            `${__dirname}/src/components/**/*.scss`
        ], 
        ['styles:start']
    ); 
});

gulp.task('fonts:copy', function() {
    gulp.src([
        `${__dirname}/src/global/fonts/**/*`
    ])
    .pipe(gulp.dest(`${__dirname}/public/fonts`));
});

gulp.task('modules:copy', function() {
    gulp.src([
        `${__dirname}/node_modules/vue/dist/vue.min.js`,
        `${__dirname}/node_modules/vue-material/dist/vue-material.min.js`
    ])
    .pipe(gulp.dest(`${__dirname}/public/scripts`));
    
    gulp.src([
        `${__dirname}/node_modules/vue-material/dist/vue-material.min.css`
    ])
    .pipe(gulp.dest(`${__dirname}/public/styles`));
});

gulp.task('fractal:start', function(){
    const server = fractal.web.server({
        sync: true
    });

    server.on('error', err => logger.error(err.message));
    return server.start().then(() => {
        logger.success(`Fractal server is now running at ${server.url}`);
    });
});

gulp.task('default', ['styles:start', 'styles:watch', 'modules:copy', 'fonts:copy', 'fractal:start']);
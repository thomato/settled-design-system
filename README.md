# Settled Design System

The Design System is built with [https://fractal.build](https://fractal.build) and works with [https://vuematerial.io](https://vuematerial.io) to provide the Vue components.

## View and edit the Design System

Install the dependencies:

```
npm install
```

Then start Fractal with:

```
npm start
```

This will start Fractal [http://localhost:3000/](http://localhost:3000/) with hot reload to sync the browser with any changes to the documentation or components.

## Build 

Run the build with:

```
npm run build
```

This will compile all the CSS (excluding the Vue Material code CSS file) and fonts into a minified CSS file in './build'.